<?php

namespace Model;

class Product_Image extends \Emagid\Core\Model
{

    static $tablename = "product_images";

    public static $fields = [
        'product_map_id',
        'image',
        'display_order'
    ];

    /**
     * checks that the image exists in the path
     *
     * @param type $size : size of an image to check if that size exists
     * @return boolean: true if file exists, false otherwise
     */
    public function exists_image($size = [])
    {
        $size_str = (count($size) == 2) ? implode("_", $size) : "";
        if ($this->image != "" && file_exists(UPLOAD_PATH . 'products' . DS . $size_str . $this->image)) {
            return true;
        }
        return false;
    }

    /**
     * builds url link to the image in the specified path.
     *
     * @param type $size : optional size to get the image with that specific size
     * @return type: url to image
     */
    public function get_image_url($size = [])
    {
        $size_str = (count($size) == 2) ? implode("_", $size) : "";
        return UPLOAD_URL . 'products/' . $size_str . $this->image;
    }

    public function getProductImage($id, $productType){
        $productMap = new Product_Map();
        if($product = $productMap->getMap($id, $productType)->id) {
            return self::getList(['where' => "product_map_id = $product", 'orderBy' => 'display_order', 'sort' => 'DESC']);
        } else {
            return '';
        }
    }
}

